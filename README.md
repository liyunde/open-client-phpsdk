# 开放平台 CLIENT PHP SDK

开放平台SDK,非对称加密开放平台客户端,类似支付宝开放平台的客户端功能,支持SOP服务网关

## 使用方式 (composer)

```
composer require liyunde/open-client-sdk
```

兼容[SOP](https://gitee.com/durcframework/SOP)网关
