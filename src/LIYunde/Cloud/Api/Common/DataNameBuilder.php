<?php
/**/
namespace LIYunde\Cloud\Api\Common;

interface DataNameBuilder
{
    const DATA_SUFFIX = '_response';

    /**
     * 构建数据节点名称
     * @param string $method 方法名
     * @return string 返回数据节点名称
     */
    public function build($method);
}
