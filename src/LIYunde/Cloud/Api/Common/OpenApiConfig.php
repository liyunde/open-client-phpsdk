<?php
/*
   Copyright (c) [2020] [liyunde]
   [open-client-phpsdk] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
 */

namespace LIYunde\Cloud\Api\Common;

use Monolog\Logger;

/**
 * @author liyunde
 *
 * Date:   2019/9/19
 * Time:   13:56
 */
class OpenApiConfig {

    /** 成功返回码值 */
    const successCode = "10000";
    /** 默认版本号 */
    const defaultVersion = "1.0";

    /** 字符编码 */
    const charset = "UTF-8";
    /** 签名方式 */
    const signType = "RSA2";
    /** 格式类型名称 */
    const formatJson = "json";

    const formatForm = "form";
    /** 时间戳格式 */
    const timestampPattern = "Y-m-d H:i:s";

    /** 接口属性名 */
    const methodName = "method";
    /** 版本号名称 */
    const versionName = "version";
    /** 编码名称 */
    const charsetName = "charset";
    /** appKey名称 */
    const appKeyName = "app_id";
    /** data名称 */
    const dataName = "biz_content";
    /** 时间戳名称 */
    const timestampName = "timestamp";
    /** 签名串名称 */
    const signName = "sign";
    /** 签名类型名称 */
    const signTypeName = "sign_type";
    /** 格式化名称 */
    const formatName = "format";
    /** accessToken名称 */
    const accessTokenName = "app_auth_token";
    /** 国际化语言 */
    const locale = "zh-CN";
    /** 响应code名称 */
    const responseCodeName = "code";
    /** 错误响应节点 */
    const errorResponseName = "error_response";

    /** 请求超时时间 */
    const connectTimeoutSeconds = 60;
    /** http读取超时时间 */
    const readTimeoutSeconds = 60;
    /** http写超时时间 */
    const writeTimeoutSeconds = 60;

    private $dataNameBuilder;

    private $userAgent;

    /**
     * @var Logger $logger
     */
    private $logger;

    /**
     * @var string
     */
    private $sdkVersion;

    /**
     * @return Logger
     */
    public function getLogger() {
        return $this->logger;
    }

    /**
     * @return string
     */
    public function getSdkVersion() {
        return $this->sdkVersion;
    }

    /**
     * @return mixed
     */
    public function getUserAgent() {
        return $this->userAgent;
    }

    public function buildUserAgent() {
        $this->userAgent = sprintf("OpenSdkPHPClient/{$this->sdkVersion} %s %s(%s/%s) ",
            date_default_timezone_get(), PHP_VERSION, PHP_OS, PHP_ZTS);
    }

    /**
     * @param Logger $logger
     */
    public function setLogger(Logger $logger) {
        $this->logger = $logger;
    }

    /**
     * @param string $sdkVersion
     */
    public function setSdkVersion($sdkVersion) {
        $this->sdkVersion = $sdkVersion;
    }

    public function getSuccessCode() {
        return self::successCode;
    }

    public function getDefaultVersion() {
        return self::defaultVersion;
    }

    public function getCharset() {
        return self::charset;
    }

    public function getSignType() {
        return self::signType;
    }

    public function getFormatJson() {
        return self::formatJson;
    }

    public function getFormatForm() {
        return self::formatForm;
    }


    public function getTimestampPattern() {
        return self::timestampPattern;
    }

    public function getMethodName() {
        return self::methodName;
    }

    public function getVersionName() {
        return self::versionName;
    }

    public function getCharsetName() {
        return self::charsetName;
    }

    public function getAppKeyName() {
        return self::appKeyName;
    }

    public function getDataName() {
        return self::dataName;
    }

    public function getTimestampName() {
        return self::timestampName;
    }

    public function getSignName() {
        return self::signName;
    }

    public function getSignTypeName() {
        return self::signTypeName;
    }

    public function getFormatName() {
        return self::formatName;
    }

    public function getAccessTokenName() {
        return self::accessTokenName;
    }

    public function getLocale() {
        return self::locale;
    }

    public function getResponseCodeName() {
        return self::responseCodeName;
    }

    public function getErrorResponseName() {
        return self::errorResponseName;
    }

    public function getConnectTimeoutSeconds() {
        return self::connectTimeoutSeconds;
    }

    public function getReadTimeoutSeconds() {
        return self::readTimeoutSeconds;
    }

    public function getWriteTimeoutSeconds() {
        return self::writeTimeoutSeconds;
    }

    public function getDataNameBuilder() {
        if (!$this->dataNameBuilder) {
            $this->dataNameBuilder = new DefaultDataNameBuilder();
        }

        return $this->dataNameBuilder;
    }
}
