<?php
/*
   Copyright (c) [2020] [liyunde]
   [open-client-phpsdk] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
 */

namespace LIYunde\Cloud\Api\Common;

/**
 * Class RequestForm
 * @author liyunde
 * @since 2020/8/23
 *
 * @package LIYunde\Cloud\Api\Common
 */
class RequestForm {

    /**
     * 请求表单内容
     * @var array
     */
    private $form;

    /** 上传文件 */
    private $files;

    private $charset;

    private $requestMethod = 'POST';

    /**
     * RequestForm constructor.
     * @param $form
     */
    public function __construct($form) {
        $this->form = $form;
    }

    /**
     * @return array
     *
     * @author liyunde
     */
    public function getForm() {
        return $this->form;
    }

    /**
     * @param mixed $form
     */
    public function setForm($form) {
        $this->form = $form;
    }

    /**
     * @return mixed
     */
    public function getFiles() {
        return $this->files;
    }

    /**
     * @param mixed $files
     */
    public function setFiles($files) {
        $this->files = $files;
    }

    /**
     * @return mixed
     */
    public function getCharset() {
        return $this->charset;
    }

    /**
     * @param mixed $charset
     */
    public function setCharset($charset) {
        $this->charset = $charset;
    }

    /**
     * @return string
     */
    public function getRequestMethod() {
        return $this->requestMethod;
    }

    /**
     * @param string $requestMethod
     */
    public function setRequestMethod($requestMethod) {
        $this->requestMethod = $requestMethod;
    }
}
