<?php
/*
   Copyright (c) [2020] [liyunde]
   [open-client-phpsdk] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
 */

namespace LIYunde\Cloud\Api\Common;

/**
 * Class DefaultDataNameBuilder
 * @author liyunde
 * @since 2020/8/23
 *
 * @package LIYunde\Cloud\Api\Common
 *
 * 将方法名中的"."转成"_"并在后面追加"_response"<br>
 * 如：gateway.echo.demo --> gateway_echo_demo_response<br>
 * <pre>
 * {
 *     "gateway_echo_demo_response": {
 *         "code": "20000",
 *         "msg": "Service Currently Unavailable",
 *         "sub_code": "isp.unknown-error",
 *         "sub_msg": "系统繁忙"
 *     },
 *     "sign": "ERIT788778JKEIJ...../+=uu"
 * }
 * </pre>
 */
class DefaultDataNameBuilder implements DataNameBuilder {

    const DOT = '.';
    const UNDERLINE = '_';

    /**
     * 构建数据节点名称
     * @param string $method 方法名
     * @return string 返回数据节点名称
     */
    public function build($method) {
        return str_replace(self::DOT, self::UNDERLINE, $method) . self::DATA_SUFFIX;
    }
}
