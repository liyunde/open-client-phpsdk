<?php
/*
   Copyright (c) [2020] [liyunde]
   [open-client-phpsdk] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
 */

namespace LIYunde\Cloud\Api\Common;

use LIYunde\Cloud\Api\Response\BaseResponse;

/**
 * Class SdkErrors
 * @author liyunde
 * @since 2020/8/23
 *
 * @package LIYunde\Cloud\Api\Common
 */
class SdkErrors {

    private $code;
    private $msg;
    private $subCode;
    private $subMsg;

    /**
     * SdkErrors constructor.
     * @param $code
     * @param $msg
     */
    public function __construct($code, $msg) {
        $this->code = $code;
        $this->msg = $msg;
        $this->subCode = $code;
        $this->subMsg = $msg;
    }

    /**
     * @return mixed | BaseResponse
     * @since 2019/9/19 15:02
     * @author liyunde
     */
    public function getErrorResponse() {
        $errorResponse = new BaseResponse();

        $errorResponse->setCode($this->code);
        $errorResponse->setMsg($this->msg);
        $errorResponse->setSubCode($this->subCode);
        $errorResponse->setSubMsg($this->subMsg);

        return $errorResponse;
    }

    public static function IoError() {
        return (new SdkErrors("3469724", "IO错误"))->getErrorResponse();
    }

    public static function NetError($msg = '网络错误', $err = 1003406561) {
        return (new SdkErrors($err, $msg))->getErrorResponse();
    }

    public static function ResponseSignatureError() {
        return (new SdkErrors("963709573", "验证服务端返回数据sign 签名错误"))->getErrorResponse();
    }

    public static function InvalidArgumentError($msg = '请求参数有错') {
        return (new SdkErrors("-196827319", $msg))->getErrorResponse();
    }

    public static function SystemError($msg = '系统错误') {
        return (new SdkErrors("985594426", $msg))->getErrorResponse();
    }

    public static function UnkonwnError($msg = '未知错误') {
        return (new SdkErrors("817495345", $msg))->getErrorResponse();
    }
}
