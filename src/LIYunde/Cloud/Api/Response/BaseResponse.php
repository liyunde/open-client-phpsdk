<?php
/*
   Copyright (c) [2020] [liyunde]
   [open-client-phpsdk] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
 */

namespace LIYunde\Cloud\Api\Response;

/**
 * Class BaseResponse
 * @author liyunde
 * @since 2020/8/23
 *
 * @package LIYunde\Cloud\Api\Response
 */
class BaseResponse implements \JsonSerializable {

    const SUCCESS = "10000";

    private $request_id;

    /**
     * @return mixed
     */
    public function getRequestId() {
        return $this->request_id;
    }

    /**
     * @param mixed $request_id
     */
    public function setRequestId($request_id) {
        $this->request_id = $request_id;
    }

    private $code;
    private $msg;
    private $sub_code;
    private $sub_msg;

    public function isSuccess() {
        return self::SUCCESS == $this->code && empty($this->sub_code);
    }

    /**
     * @return mixed
     */
    public function getCode() {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code) {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getMsg() {
        return $this->msg;
    }

    /**
     * @param mixed $msg
     */
    public function setMsg($msg) {
        $this->msg = $msg;
    }

    /**
     * @return mixed
     */
    public function getSubCode() {
        return $this->sub_code;
    }

    /**
     * @param mixed $sub_code
     */
    public function setSubCode($sub_code) {
        $this->sub_code = $sub_code;
    }

    /**
     * @return mixed
     */
    public function getSubMsg() {
        return $this->sub_msg;
    }

    /**
     * @param mixed $sub_msg
     */
    public function setSubMsg($sub_msg) {
        $this->sub_msg = $sub_msg;
    }

    public function __toString() {
        return json_encode($this->jsonSerialize(), JSON_UNESCAPED_UNICODE);
    }

    public function jsonSerialize() {
        $vars = get_object_vars($this);
        $vars['success'] = $this->isSuccess();
        return $vars;
    }
}
