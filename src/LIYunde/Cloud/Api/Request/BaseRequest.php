<?php
/*
   Copyright (c) [2020] [liyunde]
   [open-client-phpsdk] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
 */

namespace LIYunde\Cloud\Api\Request;

use LIYunde\Cloud\Api\Common\OpenApiConfig;
use LIYunde\Cloud\Api\Common\RequestForm;

/**
 * Class BaseRequest
 * @author liyunde
 * @since 2020/8/23
 *
 * @package LIYunde\Cloud\Api\Request
 */
class BaseRequest implements \JsonSerializable {

    private $apiVersion = '1.0';
    private $apiMethodName;

    private $bizContent;
    private $bizModel;

    /**
     * 上传文件
     */
    private $files;

    private $responseClass;


    public function __construct($apiMethodName = null, $apiVersion = null) {
        if ($apiVersion) {
            $this->apiVersion = $apiVersion;
        }

        if ($apiMethodName) {
            $this->apiMethodName = $apiMethodName;
        }
    }

    /**
     * @return string
     */
    public function getApiVersion() {
        return $this->apiVersion;
    }

    /**
     * @return mixed
     */
    public function getApiMethodName() {
        if (!$this->apiMethodName) {
            throw new \InvalidArgumentException("请求的接口方法名必须设置");
        }
        return $this->apiMethodName;
    }

    /**
     * @return mixed
     */
    public function getBizContent() {
        return $this->bizContent;
    }

    /**
     * @param mixed $bizContent
     */
    public function setBizContent($bizContent) {
        $this->bizContent = $bizContent;
    }

    /**
     * @return mixed
     */
    public function getBizModel() {
        return $this->bizModel;
    }

    /**
     * @param mixed $bizModel
     */
    public function setBizModel($bizModel) {
        $this->bizModel = $bizModel;
    }

    /**
     * @return mixed
     */
    public function getFiles() {
        return $this->files;
    }

    /**
     * @param mixed $files
     */
    public function setFiles($files) {
        $this->files = $files;
    }

    /**
     * @return mixed
     */
    public function getResponseClass() {
        return $this->responseClass;
    }

    /**
     * @param mixed $responseClass
     */
    public function setResponseClass($responseClass) {
        $this->responseClass = $responseClass;
    }


    /**
     * 添加上传文件
     *
     * @param $file
     */
    public function addFile($file) {
        if ($this->files == null) {
            $this->files = [];
        }
        $this->files[] = $file;
    }

    /**
     * @param OpenApiConfig $openConfig
     * @return RequestForm
     * @author liyunde
     */
    public function createRequestForm(OpenApiConfig $openConfig) {

        // 业务参数
        $bizContent = $this->buildBizContent();

        // 公共请求参数
        $params = [
            $openConfig::methodName => $this->getApiMethodName(),
            $openConfig::formatName => $openConfig::formatJson,
            $openConfig::charsetName => $openConfig::charset,
            $openConfig::signTypeName => $openConfig::signType,
            $openConfig::timestampName => date($openConfig::timestampPattern),
            $openConfig::versionName => $this->getApiVersion(),
            $openConfig::dataName => $bizContent,
        ];

        $requestForm = new RequestForm($params);

        $requestForm->setCharset($openConfig::charset);

        if ($this->files) {
            $requestForm->setFiles($this->files);
        }

        return $requestForm;
    }

    protected function buildBizContent() {
        if (!$this->bizContent && $this->bizModel) {
            $this->bizContent = json_encode($this->bizModel, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        }

        if (!$this->bizContent) {
            throw new \InvalidArgumentException("业务参数必须设置");
        }

        return $this->bizContent;
    }

    /**
     * 指定HTTP请求method,默认POST
     *
     * @return string
     */
    protected function getRequestMethod() {
        return 'POST';
    }

    public function __toString() {
        return json_encode($this->jsonSerialize(), JSON_UNESCAPED_UNICODE);
    }

    public function jsonSerialize() {
        return get_object_vars($this);
    }
}
