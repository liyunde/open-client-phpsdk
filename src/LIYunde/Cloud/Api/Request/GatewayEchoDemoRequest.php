<?php
/*
   Copyright (c) [2020] [liyunde]
   [open-client-phpsdk] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
 */

namespace LIYunde\Cloud\Api\Request;

use LIYunde\Cloud\Api\Response\GatewayEchoDemoResponse;

/**

 * @author liyunde
 *
 * Date:   2020/6/14
 * Time:   11:08
 */
class GatewayEchoDemoRequest extends BaseRequest
{

    /**
     * 网关测试接口
     */
    const METHOD_NAME = "gateway.echo.demo";

    public function getApiMethodName()
    {
        return self::METHOD_NAME;
    }

    public function getResponseClass()
    {
        return GatewayEchoDemoResponse::class;
    }
}
