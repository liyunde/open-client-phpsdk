<?php
/*
   Copyright (c) [2020] [liyunde]
   [open-client-phpsdk] is licensed under Mulan PSL v2.
   You can use this software according to the terms and conditions of the Mulan PSL v2.
   You may obtain a copy of Mulan PSL v2 at:
            http://license.coscl.org.cn/MulanPSL2
   THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT, MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
   See the Mulan PSL v2 for more details.
 */

namespace LIYunde\Cloud\Api\Error;

use Exception;

/**
 * @author liyunde
 *
 * Date:   2019/9/19
 * Time:   14:55
 */
class OpenSignException extends Exception {

    private $errCode;
    private $errMsg;

    /**
     * OpenSignException constructor.
     * @param $errCode
     * @param $errMsg
     * @param null| \Exception $e
     */
    public function __construct($errCode, $errMsg, $e = null) {
        parent::__construct("$errCode:$errMsg", intval($errCode), $e);
        $this->errCode = $errCode;
        $this->errMsg = $errMsg;
    }

    /**
     * @return mixed
     */
    public function getErrCode() {
        return $this->errCode;
    }

    /**
     * @return mixed
     */
    public function getErrMsg() {
        return $this->errMsg;
    }
}
